import { useRef, useState } from "react";

import Input from "../../ui/Input";
import style from "./MealItemForm.module.css";

const MealItemForm = (props) => {
  const refInputAmount = useRef();
  const [isValidAmount, setIsValidAmount] = useState(true);
  const handlerSubmit = (e) => {
    e.preventDefault();
    const enteredAmount = refInputAmount.current.value;
    const enteredAmountNumber = +enteredAmount;
    if (
      enteredAmount.trim().length === 0 ||
      enteredAmountNumber < 1 ||
      enteredAmountNumber > 5
    ) {
      setIsValidAmount(false);
      return;
    }
    props.addToCart(enteredAmountNumber);
  };
  return (
    <form className={style.form} onSubmit={handlerSubmit}>
      <Input
        ref={refInputAmount}
        label="Amount"
        input={{
          id: "amount_" + props.id,
          type: "number",
          min: 1,
          max: 5,
          step: "1",
          defaultValue: 1,
        }}
      />
      <button type="submit">+ Add</button>
      {!isValidAmount && <p>Please enter a valid amount (1-5)</p>}
    </form>
  );
};

export default MealItemForm;
