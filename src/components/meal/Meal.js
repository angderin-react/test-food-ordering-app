import AvailableMeal from "./AvailableMeal";
import MealSummary from "./MealSummary";

const Meal = (props) => {
  return (
    <>
      <MealSummary />
      <AvailableMeal />
    </>
  );
};

export default Meal;
