import { useContext, useEffect, useState } from "react";
import CartContext from "../../context/cart-context";

import CartIcon from "../cart/CartIcon";

import style from "./HeaderCartButton.module.css";

const HeaderCartButton = (props) => {
  const ctxCart = useContext(CartContext);
  const [btnAnim, setBtnAnim] = useState(false);

  const { items } = ctxCart;

  const nCartItem = items.reduce((prev, item) => {
    return prev + item.amount;
  }, 0);

  const btnClasses = `${style.button} ${btnAnim ? style.bump : ""}`;

  useEffect(() => {
    if (items.length === 0) {
      return;
    }
    setBtnAnim(true);
    const timer = setTimeout(() => {
      setBtnAnim(false);
    }, 300);
    return () => {
      clearTimeout(timer);
    };
  }, [items]);

  return (
    <button className={btnClasses} onClick={props.onClick}>
      <span className={style.icon}>
        <CartIcon />
      </span>
      <span>Your Cart</span>
      <span className={style.badge}>{nCartItem}</span>
    </button>
  );
};

export default HeaderCartButton;
