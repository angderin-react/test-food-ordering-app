import styleHeader from "./Header.module.css";
import headerImg from "../../assets/meals.jpg";
import HeaderCartButton from "./HeaderCartButton";
const Header = (props) => {
  return (
    <>
      <header className={styleHeader.header}>
        <h1>ReactMeals</h1>
        <HeaderCartButton onClick={props.onShowCart} />
      </header>
      <div className={styleHeader["main-image"]}>
        <img src={headerImg} alt="A table full of delicious food!" />
      </div>
    </>
  );
};

export default Header;
