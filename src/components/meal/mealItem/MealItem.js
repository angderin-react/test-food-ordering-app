import { useContext } from "react";
import CartContext from "../../../context/cart-context";

import style from "./MealItem.module.css";
import MealItemForm from "./MealItemForm";

const MealItem = (props) => {
  const ctxCart = useContext(CartContext);
  const price = `$ ${props.price}`;

  const handlerAddToCart = (amount) => {
    ctxCart.addItem({
      id: props.id,
      name: props.name,
      amount: amount,
      price: props.price,
    });
  };

  return (
    <li className={style.meal}>
      <div>
        <h3>{props.name}</h3>
        <div className={style.description}>{props.description}</div>
        <div className={style.price}>{price}</div>
      </div>
      <div>
        <MealItemForm id={props.id} addToCart={handlerAddToCart} />
      </div>
    </li>
  );
};

export default MealItem;
