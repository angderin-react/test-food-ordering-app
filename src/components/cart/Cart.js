import { useContext } from "react";
import CartContext from "../../context/cart-context";

import Modal from "../ui/Modal";
import style from "./Cart.module.css";
import CartItem from "./CartItem";

const Cart = (props) => {
  const ctxCart = useContext(CartContext);

  const totalAmount = `$ ${ctxCart.totalAmount.toFixed(2)}`;
  const hasItem = ctxCart.items.length > 0;

  const handlerRemoveCart = (id) => {
    ctxCart.removeItem(id)
  };

  const handlerAddCart = (item) => {
    ctxCart.addItem({ ...item, amount: 1 });
  };

  console.log(ctxCart.items);
  const viewCartItem = (
    <ul className={style["cart-items"]}>
      {ctxCart.items.map((item) => {
        return (
          <CartItem
            key={item.id}
            name={item.name}
            price={item.price}
            amount={item.amount}
            onRemove={handlerRemoveCart.bind(null, item.id)}
            onAdd={handlerAddCart.bind(null, item)}
          ></CartItem>
        );
      })}
    </ul>
  );

  return (
    <Modal onClose={props.onHideCart}>
      {viewCartItem}
      <div className={style.total}>
        <span>Total Amount</span>
        <span>{totalAmount}</span>
      </div>
      <div className={style.actions}>
        <button className={style["button--alt"]} onClick={props.onHideCart}>
          Close
        </button>
        {hasItem && <button className={style.button}>Order</button>}
      </div>
    </Modal>
  );
};

export default Cart;
