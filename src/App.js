import { useState } from "react";

import Cart from "./components/cart/Cart";
import Header from "./components/layout/Header";
import Meal from "./components/meal/Meal";
import CartProvider from "./context/CartProvider";

function App() {
  const [cartIsShown, setCartIsShown] = useState(false);

  const handlerShowCart = () => {
    setCartIsShown(true);
  };

  const handlerHideCart = () => {
    setCartIsShown(false);
  };

  return (
    <CartProvider>
      {cartIsShown && <Cart onHideCart={handlerHideCart} />}
      <Header onShowCart={handlerShowCart} />
      <main>
        <Meal />
      </main>
    </CartProvider>
  );
}

export default App;
