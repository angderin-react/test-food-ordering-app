import ReactDOM from "react-dom";

import style from "./Modal.module.css";

const Backdrop = (props) => {
  return <div className={style.backdrop} onClick={props.onClose}></div>;
};

const ModalOverlay = (props) => {
  return (
    <div className={style.modal}>
      <div className={style.content}>{props.children}</div>
    </div>
  );
};

const portalOverlay = document.getElementById('overlays')

const Modal = (props) => {
  return (
    <>
      {ReactDOM.createPortal(<Backdrop onClose={props.onClose}/>, portalOverlay)}
      {ReactDOM.createPortal(<ModalOverlay>{props.children}</ModalOverlay>, portalOverlay)}
    </>
  );
};

export default Modal;
